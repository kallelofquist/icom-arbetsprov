package models;

import java.util.List;

public class Person {
    public String firstname;
    public String lastname;
    public Address address;
    public Phone phone;
    public List<Family> family;
}

