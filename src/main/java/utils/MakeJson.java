package utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import models.Address;
import models.Family;
import models.Person;
import models.Phone;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.ArrayList;
import java.util.List;

/**
 * JSON convert class. Help class for converting string into JSON
 *
 * Author - Karl Löfquist
 * Version - 1.0.0
 */
public class MakeJson {

    /**
     * Method that converts String list into JSON
     *
     * @param stringList - List of strings to be converted to JSON
     * @return - JSON Object containing JSON array of people
     */
    public static JSONObject convertListToJSON (List<String> stringList) {

        JSONObject masterJSON = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        JSONParser parser = new JSONParser();
        ObjectMapper mapper = new ObjectMapper();

        Person person;
        Phone phone;
        Address address;
        Family family;

        List<Person> people = new ArrayList<>();
        List<Family> familyList = new ArrayList<>();

        String previousPrefix = "";

        JSONObject jsonPerson;

        for (String string : stringList) {
            String[] elements = string.split("\\|");
            switch (elements[0]) {
                case "P" -> {
                    person = new Person();
                    person.firstname = elements[1];
                    person.lastname = elements[2];
                    previousPrefix = elements[0];
                    people.add(person);
                }
                case "T" -> {
                    phone = new Phone();
                    phone.mobile = elements[1];
                    phone.home = elements[2];
                    if (previousPrefix.equals("P")) {
                        int index = (people.size() - 1);
                        people.get(index).phone = phone;
                    }
                    else if (previousPrefix.equals("F")) {
                        int index = (familyList.size() - 1);
                        familyList.get(index).phone = phone;
                    }
                }
                case "A" -> {
                    address = new Address();
                    address.street = elements[1];
                    address.city = elements[2];
                    if (elements.length > 3) {
                        address.postalCode = Integer.parseInt(elements[3]);
                    }
                    if (previousPrefix.equals("P")) {
                        int index = (people.size() - 1);
                        people.get(index).address = address;
                    }
                    else if (previousPrefix.equals("F")) {
                        int index = (familyList.size() - 1);
                        familyList.get(index).address = address;
                    }
                }
                case "F" -> {
                    family = new Family();
                    family.name = elements[1];
                    family.born = Integer.parseInt(elements[2]);
                    familyList.add(family);
                    previousPrefix = elements[0];

                    int index = (people.size() - 1);
                    people.get(index).family = familyList;

                }
                default -> System.out.println("Something went wrong");
            }
        }

        try {
            for (Person per : people) {
                jsonPerson = (JSONObject) parser.parse(mapper.writeValueAsString(per));
                jsonArray.add(jsonPerson);
            }
        } catch (ParseException | JsonProcessingException e) {
            e.printStackTrace();
        }

        masterJSON.put("people", jsonArray);

        return masterJSON;
    }
}
