package utils;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.util.List;

/**
 * XML convert class. Help class for converting string list into XML
 *
 * Author - Karl Löfquist
 * Version - 1.0.0
 */
public class MakeXml {

    /**
     * Method for converting a list of strings into a formatted XML string
     *
     * @param stringList - List of strings to be converted into a full XML string
     * @return - String that is formatted XML
     */
    public static String convertListToXML (List<String> stringList) {

        String xmlString = "";
        String previousPrefix = "";

        try {
            StringWriter stringWriter = new StringWriter();

            XMLOutputFactory xMLOutputFactory = XMLOutputFactory.newInstance();
            XMLStreamWriter xMLStreamWriter = xMLOutputFactory.createXMLStreamWriter(stringWriter);

            xMLStreamWriter.writeStartDocument();
            xMLStreamWriter.writeStartElement("people");

            for (String string : stringList) {

            String[] elements = string.split("\\|");

                switch (elements[0]) {
                    case "P" -> {
                        if (!previousPrefix.equals("")) {
                            xMLStreamWriter.writeEndElement();
                        }
                        if (previousPrefix.equals("F")) {
                            xMLStreamWriter.writeEndElement();
                        }
                        xMLStreamWriter.writeStartElement("person");
                        xMLStreamWriter.writeStartElement("firstname");
                        xMLStreamWriter.writeCharacters(elements[1]);
                        xMLStreamWriter.writeEndElement();
                        xMLStreamWriter.writeStartElement("lastname");
                        xMLStreamWriter.writeCharacters(elements[2]);
                        xMLStreamWriter.writeEndElement();
                        previousPrefix = "P";
                    }
                    case "T" -> {
                        xMLStreamWriter.writeStartElement("phone");
                        xMLStreamWriter.writeStartElement("mobile");
                        xMLStreamWriter.writeCharacters(elements[1]);
                        xMLStreamWriter.writeEndElement();
                        xMLStreamWriter.writeStartElement("home");
                        xMLStreamWriter.writeCharacters(elements[2]);
                        xMLStreamWriter.writeEndElement();
                        xMLStreamWriter.writeEndElement();
                    }
                    case "A" -> {
                        xMLStreamWriter.writeStartElement("address");
                        xMLStreamWriter.writeStartElement("street");
                        xMLStreamWriter.writeCharacters(elements[1]);
                        xMLStreamWriter.writeEndElement();
                        xMLStreamWriter.writeStartElement("city");
                        xMLStreamWriter.writeCharacters(elements[2]);
                        xMLStreamWriter.writeEndElement();
                        if (elements.length > 3) {
                            xMLStreamWriter.writeStartElement("postalcode");
                            xMLStreamWriter.writeCharacters(elements[3]);
                            xMLStreamWriter.writeEndElement();
                        }
                        xMLStreamWriter.writeEndElement();
                    }
                    case "F" -> {
                        if (previousPrefix.equals("F")) {
                            xMLStreamWriter.writeEndElement();
                        }
                        xMLStreamWriter.writeStartElement("family");
                        xMLStreamWriter.writeStartElement("name");
                        xMLStreamWriter.writeCharacters(elements[1]);
                        xMLStreamWriter.writeEndElement();
                        xMLStreamWriter.writeStartElement("born");
                        xMLStreamWriter.writeCharacters(elements[2]);
                        xMLStreamWriter.writeEndElement();
                        previousPrefix = elements[0];
                    }
                    default -> System.out.println("Something went wrong");
                }
            }

            xMLStreamWriter.writeEndElement();
            xMLStreamWriter.writeEndDocument();

            xMLStreamWriter.flush();
            xMLStreamWriter.close();

            xmlString = stringWriter.getBuffer().toString();

            stringWriter.close();

        } catch (XMLStreamException | IOException e) {
                e.printStackTrace();
        }

        return xmlString;
    }
}

