package init;

import utils.MakeJson;
import utils.MakeXml;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Program class. Main class for program
 *
 * Author - Karl Löfquist
 * Version - 1.0.0
 */
public class Program {

    /**
     * Main method that initiates program on start
     *
     * @param args - unused string arguments
     */
    public static void main(String[] args) {
        List<String> input = readInput();
        printResults(input);
    }

    /**
     * Method that takes input strings from the user
     *
     * @return - List of string inputs
     */
    private static List<String> readInput() {

        // create a BufferedReader using System.in
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        List<String> inputList = new ArrayList<>();
        String currentInput;
        String previousInput = "";
        boolean continueFlag = true;

        while (continueFlag) {

            System.out.println("Enter input string or DONE if finished:");
            currentInput = "";

            try {
                currentInput = reader.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }

            Boolean inputOk = validateInput(currentInput, previousInput);

            if (inputOk) {

                if (currentInput.toUpperCase().trim().equals("DONE")) {
                    continueFlag = false;
                }
                else {
                    inputList.add(currentInput);
                    System.out.println("Input so far: " + inputList);
                    previousInput = currentInput;
                }
            }

            else {
                System.out.println("input is not OK");
            }
        }

        return inputList;
    }

    /**
     * Method for checking the users input against a regular expression, if the input comes in the right order or if the input
     * is equal to 'DONE'
     *
     * @param currentInput - String that the user just wrote
     * @param previousInput - String that the user previously wrote
     * @return - Boolean - True if input is OK, otherwise false
     */
    private static Boolean validateInput( String currentInput, String previousInput ) {

        currentInput = currentInput.trim();

        // Regular expression for currentInput validation.
        String regExp = "^[PTAF]\\|(.[\\w\\d\\-|\\s,.].*)?";

        if (currentInput.matches(regExp)) {
            if (!previousInput.isEmpty() && (previousInput.charAt(0) == 'P' && (currentInput.charAt(0) == 'T' || currentInput.charAt(0) == 'A' || currentInput.charAt(0) == 'F'))) {
                return true;
            }
            else if (!previousInput.isEmpty() && (previousInput.charAt(0) == 'F' && (currentInput.charAt(0) == 'T' || currentInput.charAt(0) == 'A'))) {
                return true;
            }
            else if (currentInput.charAt(0) == 'P') {
                return true;
            }
            else return !previousInput.isEmpty() && (previousInput.charAt(0) == 'T' || previousInput.charAt(0) == 'A');
        }
        else return currentInput.toUpperCase().trim().equals("DONE");

    }

    /**
     * Method for printing results based on user choice (JSON or XML)
     *
     * @param output - List of validated strings to be transformed into either JSON or XML
     */
    private static void printResults( List<String> output ) {

        // create a BufferedReader using System.in
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Get results in JSON or XML?");
        String outputFormat = null;

        try {
            outputFormat = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

        switch (Objects.requireNonNull(outputFormat).toUpperCase().trim()) {
            case "JSON" -> System.out.println(MakeJson.convertListToJSON(output).toJSONString());
            case "XML" -> System.out.println(MakeXml.convertListToXML(output));
            default -> System.out.println("Could not recognize desired output format");
        }
    }
}